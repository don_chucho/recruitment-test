import { Component, OnInit } from '@angular/core';
/**
 * Welcome screen.
 * @author David Galvis <dav.galvis@gmail.com>
 * @copyright Copyright (c) 2020
 */

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
